# DiskCopy

Copy one disk or image file to an other


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DISKCOPY.LSM

<table>
<tr><td>title</td><td>DiskCopy</td></tr>
<tr><td>version</td><td>beta 0.95 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2008-07-23</td></tr>
<tr><td>description</td><td>Copy one disk or image file to an other</td></tr>
<tr><td>keywords</td><td>Freedos, disk, diskcopy, copy</td></tr>
<tr><td>author</td><td>"Imre Leber" &lt;imre.leber@worldonline.be&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>"Imre Leber" &lt;imre.leber@worldonline.be&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://users.telenet.be/imre/FreeDOS/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Diskcopy</td></tr>
</table>
